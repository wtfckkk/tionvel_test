# Aplicación para seleccion de personal Tionvel

* Desarrollador: Tomás Cornejo C.
* Fecha Inicio: 29/01/2018
* Fecha Término: 29/01/2018

## Instalación de la Aplicación

Correr este comando desde el directorio donde se desea instalar Slim Framework

    composer require slim/slim "^3.0"

Correr este comando para iniciar aplicacion	

	php -S localhost:8080 -t public public/index.php

Ingresar desde un navegador a 

	localhost:8080

Y ya está! 

Cualquier duda y/o problema con la instalación favor consultar documentación 
https://www.slimframework.com/docs/
