<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});

$app->post('/date/', function (Request $request, Response $response, array $args) {

	//Get data post
	$data = $request->getParsedBody();

    // Render index view
    return $this->renderer->render($response, 'dates.phtml', $data);
    
});

$app->post('/calculate/', function (Request $request, Response $response, array $args) {
	
    //Get data post
	$data = $request->getParsedBody();

    //Validate & Calculate days for each date
    for ($i=0; $i < count($data['fecha']) ; $i++) { 

        //Validate        
        if ($i>0) {
            //Transform dates to compare
            $date0 = \DateTime::createFromFormat("Y-m-d",$data['fecha'][$i-1]);
            $date1 = \DateTime::createFromFormat("Y-m-d",$data['fecha'][$i]);
            if ($date1 < $date0) {
                // Render nok to view in JSON                
                $data = array('status' => 'nok', 'desc' => 'La fecha '.($i+1).' es superior a la fecha '.$i.', favor revisar formulario');
                return $response->withJson($data);
            }    
        }
        
        //Add weekdays to valid date 
        $data['result_date'][$i] = date("d-m-Y", strtotime(date($data['fecha'][$i]). ' + '.$data['numero'][$i].' weekdays'));        
    }

    //Return ok to view in phtml
    return $this->renderer->render($response, 'calculate.phtml', $data);
    
});
